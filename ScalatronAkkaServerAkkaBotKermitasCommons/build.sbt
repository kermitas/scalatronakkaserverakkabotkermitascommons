name := "ScalatronAkkaServerAkkaBotKermitasCommons"

version := "0.1"

organization := "kermitas"

scalaVersion := "2.10.1"

// collect all dependencies
retrieveManaged := true

// --- akka
libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.2-M2"

// for debugging sbt problems
logLevel := Level.Debug

