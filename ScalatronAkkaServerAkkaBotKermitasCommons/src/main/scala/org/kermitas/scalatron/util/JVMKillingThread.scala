package main.scala.org.kermitas.scalatron.util
{
  class JVMKillingThread( jvmStopDelayInSeconds : Int , trueIfSystemExitFalseIfRuntimeHalt : Boolean ) extends Thread
  {
    val log = java.util.logging.Logger.getLogger( getClass.getName )

    setDaemon( true )

    override def run
    {
      var logMessage = ""

      logMessage = "Setting up JVM killer: " + jvmStopDelayInSeconds + " seconds"
      log.warning( logMessage )
      println( logMessage )

      Thread.sleep( 1000 )

      for ( i <- 0 to jvmStopDelayInSeconds )
      {
        logMessage = "Will stop JVM in " + (jvmStopDelayInSeconds-i) + " s..."
        log.warning( logMessage )
        println( logMessage )

        Thread.sleep( 1000 )
      }

      logMessage = "Stopping JVM..." + (if( trueIfSystemExitFalseIfRuntimeHalt ) "System.exit( -1 )" else "Runtime.getRuntime.halt( -1 )")
      log.warning( logMessage )
      println( logMessage )

      if ( trueIfSystemExitFalseIfRuntimeHalt )
        System.exit( -1 )
      else
        Runtime.getRuntime.halt( -1 )
    }
  }
}
