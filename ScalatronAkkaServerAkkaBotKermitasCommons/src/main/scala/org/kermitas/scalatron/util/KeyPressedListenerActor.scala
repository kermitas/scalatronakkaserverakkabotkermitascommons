package main.scala.org.kermitas.scalatron.util
{
  import akka.actor._
  import akka.event._
  import scala.concurrent.duration._

  object KeyPressedListenerActor
  {
    // ----

    sealed trait State
      case object Uninitialized extends State
      case object WaitingForAKey extends State

    // ----

    sealed trait StateData
      case object UninitializedStateData extends StateData
      case class WaitingForAKeyStateData( listenerActor : ActorRef , message : Any ) extends StateData

    // ----

    sealed trait Messages

      sealed trait IncomingMessages extends Messages

        case class Init( listenerActor : ActorRef , message : Any , timeIntervalInMs : Int ) extends IncomingMessages

    // ----
  }

  class KeyPressedListenerActor extends Actor with FSM[KeyPressedListenerActor.State, KeyPressedListenerActor.StateData]
  {
    startWith(KeyPressedListenerActor.Uninitialized, KeyPressedListenerActor.UninitializedStateData)

    when( KeyPressedListenerActor.Uninitialized )
    {
      case Event( KeyPressedListenerActor.Init( listenerActor , message , timeIntervalInMs ) , KeyPressedListenerActor.UninitializedStateData ) ⇒
      {
        setStateTimeout( KeyPressedListenerActor.WaitingForAKey , Some( timeIntervalInMs millisecond ) )

        goto( KeyPressedListenerActor.WaitingForAKey ) using new KeyPressedListenerActor.WaitingForAKeyStateData( listenerActor , message )
      }
    }

    when( KeyPressedListenerActor.WaitingForAKey )
    {
      case Event( StateTimeout , stateData : KeyPressedListenerActor.WaitingForAKeyStateData ) ⇒
      {
        if ( System.in.available > 0 )
        {
          stateData.listenerActor ! stateData.message
          stop( FSM.Normal )
        }
        else
        {
          stay using stateData
        }
      }
    }

    onTransition
    {
      case fromState -> toState ⇒ log.debug( "Change state from " + fromState + " to " + toState )
    }

    whenUnhandled
    {
      case Event( unknownMessage , stateData ) ⇒
      {
        log.warning( "Received unknown message '" + unknownMessage + "' (state data " + stateData + ")" )
        stay using stateData
      }
    }

    onTermination
    {
      case StopEvent(stopType, state, stateData) ⇒
      {
        stopType match
        {
          case FSM.Normal ⇒ log.info( "Stopping (normal), state " + state + ", data " + stateData )
          case FSM.Shutdown ⇒ log.info( "Stopping (shutdown), state " + state + ", data " + stateData )
          case FSM.Failure(cause) ⇒ log.warning( "Stopping (failure = " + cause + "), state " + state + ", data " + stateData )
        }
      }
    }

    initialize
  }
}
