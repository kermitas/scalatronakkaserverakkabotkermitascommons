package main.scala.org.kermitas.scalatron.util
{
  import _root_.akka.actor._
  import _root_.akka.event._
  import scala.concurrent.duration._

  object MessageBroadcaster
  {
    trait Messages
      trait IncomingMessages extends Messages
        case class Init( checkForTerminatedActorsIntervalInSeconds : Int ) extends IncomingMessages
        case class Register( listener : ActorRef , classifier : (Any)=>Boolean ) extends IncomingMessages
        case class UnregisterClassifier( listener : ActorRef , classifier : (Any)=>Boolean ) extends IncomingMessages
        case class Unregister( listener : ActorRef ) extends IncomingMessages
        case object RemoveTerminatedActors extends IncomingMessages
  }

  class MessageBroadcaster extends Actor
  {
    protected val log = Logging(context.system, this)
    protected val map = new scala.collection.mutable.HashMap[ ActorRef , scala.collection.mutable.ListBuffer[ (Any)=>Boolean ] ]
    protected var checkForTerminatedActorsIntervalInSeconds = 20

    protected var terminatedActorsChecker = createTerminatedActorsCheckTimer( checkForTerminatedActorsIntervalInSeconds )

    def receive =
    {
      // ----

      case MessageBroadcaster.Init( checkForTerminatedActorsIntervalInSeconds) ⇒
      {
        this.checkForTerminatedActorsIntervalInSeconds = checkForTerminatedActorsIntervalInSeconds
        terminatedActorsChecker.cancel
        terminatedActorsChecker = createTerminatedActorsCheckTimer( checkForTerminatedActorsIntervalInSeconds )
      }

      // ----

      case MessageBroadcaster.Register( listener , classifier ) ⇒
      {
        log.debug( "Registering listener " + listener + " with classifier " + classifier )

        map.get( listener ) match
        {
          case Some( classifiers ) ⇒
          {
            classifiers += classifier
          }

          case None ⇒
          {
            val classifiers = new scala.collection.mutable.ListBuffer[ (Any)=>Boolean ]
            map.put( listener , classifiers )
            classifiers += classifier
          }
        }
      }

      // ----

      case MessageBroadcaster.UnregisterClassifier( listener , classifier ) ⇒
      {
        log.debug( "Unregistering classifier " + classifier + " for listener " + listener )

        map.get( listener ) match
        {
          case Some( classifiers ) ⇒
          {
            classifiers -= classifier
            if( classifiers.isEmpty ) map.remove( listener )
          }

          case None ⇒
        }
      }

      // ----

      case MessageBroadcaster.Unregister( listener ) ⇒
      {
        log.debug( "Unregistering listener " + listener )

        map.remove( listener )
      }

      // ----

      case MessageBroadcaster.RemoveTerminatedActors ⇒
      {
        val keysToRemove = new scala.collection.mutable.ListBuffer[ ActorRef ]
        map.keys.foreach( listener => if( listener.isTerminated ) keysToRemove += listener )
        keysToRemove.foreach( map.remove )

      }

      // ----

      case messageToBroadcast : AnyRef ⇒
      {
        terminatedActorsChecker.cancel

        val keysToRemove = new scala.collection.mutable.ListBuffer[ ActorRef ]

        map.iterator.foreach{ listenerWithClassifiers =>

          if( listenerWithClassifiers._1.isTerminated )
          {
            keysToRemove += listenerWithClassifiers._1
          }
          else
          {
            listenerWithClassifiers._2.find{ classifier =>
              try
              {
                if( classifier( messageToBroadcast ) )
                {
                  listenerWithClassifiers._1.tell( messageToBroadcast , sender )
                  true
                }
                else
                  false
              }
              catch{ case t : Throwable ⇒ false }
            }
          }

        }

        keysToRemove.foreach( map.remove )

        terminatedActorsChecker = createTerminatedActorsCheckTimer( checkForTerminatedActorsIntervalInSeconds )
      }

        // ----
    }

    protected def createTerminatedActorsCheckTimer( checkForTerminatedActorsIntervalInSeconds : Int ) =
    {
      context.system.scheduler.schedule( checkForTerminatedActorsIntervalInSeconds second , checkForTerminatedActorsIntervalInSeconds second , self , MessageBroadcaster.RemoveTerminatedActors )( context.system.dispatcher )
    }
  }
}