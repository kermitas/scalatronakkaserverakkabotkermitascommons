package main.scala.org.kermitas.scalatron.util
{
  import _root_.akka.actor._
  import _root_.akka.event._

  object MessageBroadcasterMonitor
  {
    trait Message
      trait IncomingMessage extends Message
        case class Init( messageBroadcaster : ActorRef ) extends IncomingMessage
  }

  class MessageBroadcasterMonitor extends Actor
  {
    protected val log = Logging(context.system, this)
    protected var messageBroadcaster : ActorRef = null

    def receive =
    {
      case MessageBroadcasterMonitor.Init( messageBroadcaster ) ⇒
      {
        this.messageBroadcaster = messageBroadcaster
        messageBroadcaster ! new MessageBroadcaster.Register( self , (m)=>true )
      }

      case message ⇒
      {
        log.debug( "Received message: " + message )
      }
    }
  }
}