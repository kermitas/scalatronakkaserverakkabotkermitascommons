import java.awt.*;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import javax.swing.*;
import javax.swing.*;
import java.awt.*;
import java.awt.EventQueue;
import java.awt.geom.Rectangle2D;

public class Main {


    public static void main(String[] args) {
        new Kalejdoskop(600, 600,
                5,
                Color.YELLOW,
                Color.GREEN,
                1, 5);
    }
}



class MyPanel extends JPanel {
    int szerokoscOkna = Kalejdoskop.szerokoscOkna;
    int wysokoscOkna = Kalejdoskop.wysokoscOkna;
    double kat = 90;
    double wspolczynnik = kat * Kalejdoskop.stosunekWspolczynnika1 / Kalejdoskop.stosunekWspolczynnika2;

    public MyPanel() {
        setPreferredSize(new Dimension(szerokoscOkna, wysokoscOkna));
    }





    protected void paintComponent(Graphics g) {

        int szerokoscOkna = this.getWidth();
        int wysokoscOkna = this.getHeight();

        System.out.println(Kalejdoskop.kwadraty);

        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;

        int verticalCenter = this.getHeight() / 2;


        double rogKwadratu = verticalCenter - (szerokoscOkna / 2);
        double bok = szerokoscOkna;
        int i = 0;
        int jakiKolor = 0;


        Rectangle2D kwad = new Rectangle2D.Double(rogKwadratu, rogKwadratu, szerokoscOkna, szerokoscOkna);
        g2d.setPaint(Kalejdoskop.pierwszyKolor);
        g2d.fill(kwad);


        while (i < Kalejdoskop.kwadraty) {
            if (jakiKolor == 0) {
                bok = (bok * Math.tan(Math.toRadians(wspolczynnik))) / ((Math.sin(Math.toRadians(wspolczynnik))) * (1 + Math.tan(Math.toRadians(wspolczynnik))));
                rogKwadratu = verticalCenter - (bok / 2);


                g2d.rotate(Math.toRadians(wspolczynnik), szerokoscOkna / 2, szerokoscOkna / 2);
                kwad = new Rectangle2D.Double(rogKwadratu, rogKwadratu, bok, bok);


                g2d.setPaint(Kalejdoskop.drugiKolor);

                g2d.fill(kwad);


                jakiKolor = 1;
            }


            if (jakiKolor == 1) {
                bok = (bok * Math.tan(Math.toRadians(wspolczynnik))) / ((Math.sin(Math.toRadians(wspolczynnik))) * (1 + Math.tan(Math.toRadians(wspolczynnik))));
                rogKwadratu = verticalCenter - (bok / 2);


                g2d.rotate(Math.toRadians(wspolczynnik), szerokoscOkna / 2, szerokoscOkna / 2);
                kwad = new Rectangle2D.Double(rogKwadratu, rogKwadratu, bok, bok);


                g2d.setPaint(Kalejdoskop.pierwszyKolor);

                g2d.fill(kwad);


                jakiKolor = 0;
            }

            i++;
        }


    }

}


class MyFrame extends JFrame {
    public MyFrame() {
        super("Kalejdoskop");
        JPanel panel = new MyPanel();

        add(panel);

        pack();


        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
}


class Kalejdoskop extends JPanel {
    static int szerokoscOkna = 0;
    static int wysokoscOkna = 0;
    static int kwadraty = 0;
    static Color pierwszyKolor = Color.RED;
    static Color drugiKolor = Color.YELLOW;
    static int stosunekWspolczynnika1 = 0;
    static int stosunekWspolczynnika2 = 0;


    public Kalejdoskop(int szerokosc, int wysokosc, int liczbaKwadratow, Color a, Color b, int stosunek1, int stosunek2) {

        szerokoscOkna = szerokosc;
        wysokoscOkna = wysokosc;
        kwadraty = liczbaKwadratow;
        pierwszyKolor = a;
        drugiKolor = b;
        stosunekWspolczynnika1 = stosunek1;
        stosunekWspolczynnika2 = stosunek2;



        jedziesz();



    }


    public static void jedziesz(){

        EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                new MyFrame();
            }
        });

    }

}